A m'asseoir sur un banc cinq minutes avec toi 
Et regarder les gens tant qu'y en a 
Te parler du bon temps qu'est mort ou qui r'viendra 
En serrant dans ma main tes p'tits doigts 
Pi donner à bouffer à des pigeons idiots 
Leur filer des coups d'pied pour de faux 
Et entendre ton rire qui lézarde les murs 
Qui sait surtout guérir mes blessures 
Te raconter un peu comment j'étais, mino 
Les bombecs fabuleux qu'on piquait chez l'marchand 
Car-en-sac et Mintho caramels à un franc 
Et les Mistral gagnants 

A marcher sous la pluie cinq minutes avec toi 
Et regarder la vie tant qu'y en a 
Te raconter la terre en te bouffant des yeux 
Te parler de ta mère un p'tit peu 
Et sauter dans les flaques pour la faire râler 
Bousiller nos godasses et s'marrer 
Et entendre ton rire comme on entend la mer 


S'arrêter, repartir en arrière 
Te raconter surtout les carambars d'antan et les coco-boers 
Et les vrais roudoudous qui nous coupaient les lèvres et nous niquaient les dents 
Et les Mistral gagnants 

A m'asseoir sur un banc cinq minutes avec toi 
Regarder le soleil qui s'en va 
Te parler du bon temps qu'est mort et je m'en fous 
Te dire que les méchants c'est pas nous 
Que si moi je suis barge ce n'est que de tes yeux 
Car ils ont l'avantage d'être deux 
Et entendre ton rire s'envoler aussi haut 
Que s'envolent les cris des oiseaux 
Te raconter enfin qu'il faut aimer la vie et l'aimer même si 
Le temps est assassin et emporte avec lui 
Les rires des enfants et les mistral gagnants 
